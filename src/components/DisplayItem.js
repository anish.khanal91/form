import React, { Component } from 'react';

class DisplayItem extends Component {
    render() {
        return (
            <tr> 
                <td scope="row">{this.props.itemElements.id}</td>
                <td>{this.props.itemElements.full_name}</td>
                <td>{this.props.itemElements.phoneNumber}</td>
                <td>{this.props.itemElements.email}</td>
                <td>{this.props.itemElements.address}</td>
                <td><span style={{color: 'red '}} onClick={() => this.props.deleteItem(this.props.itemElements.id)}>Delete</span></td>
            </tr>
        );
    }
}

export default DisplayItem ;