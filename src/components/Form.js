import React, { Component } from 'react';
import DisplayComponent from './DisplayComponent' ;

class Form extends Component {

    state = {
        id: 0,
        Employee_name: "",
        phone_no:"",
        email:"",
        address:"",
        personalInformation :[] ,
        validationMessage:"",
        validationMessageForName:""
    }
    handleChange(e) {
        let { name } = e.target;
        let { value } = e.target;
        if (name === "Employee_name" ) {
        this.setState({
            Employee_name: value
        })
    }else if( name==="phone_no"){
        this.setState({
            phone_no: value
        })
        } else if( name==="email"){
            this.setState({
                email: value
            })
        }else if( name==="address"){
            this.setState({
                address: value
            })
            }
      
    }

    handleDelete = (id) => {
        this.setState({ personalInformation: this.state.personalInformation.filter(function( obj ) {
                return obj.id !== id;
            })
        });

    }

    showData() {
        return <DisplayComponent handleDelete={this.handleDelete} myArray={this.state.personalInformation} />
    }

    handleSubmit() {
        let { personalInformation, id,Employee_name, phone_no, email, address } = this.state;

        if(Employee_name === "") {
            this.setState({
                validationMessageForName:"Name is required"
            })
        } else {
            id = id + 1;

            personalInformation.push({
                id: id,
                full_name: this.state.Employee_name,
                phoneNumber: this.state.phone_no,
                email: this.state.email,
                address: this.state.address
            });
        }

        this.setState({
            id,
            personalInformation
        });
    }

    render() {
        return (
            <div>
                <div className="form-setup">
                
                <div className="form-group">
                    <label>Name</label>

                    <input className="form-control" type ="text" name="Employee_name" value={this.state.Employee_name} onChange={(e) => this.handleChange(e)} />
                </div>

                <div className="form-group">
                    <label>Phone number</label>

                    <input className="form-control" type="number" name="phone_no" values={this.state.phone_no} onChange={(e) => this.handleChange(e)} />
                </div>

                <div className="form-group">
                    <label>Email</label>

                    <input className="form-control" type="email" name="email" values={this.state.email} onChange={(e) => this.handleChange(e)} />
                </div>

                <div className="form-group">
                    <label>address</label>
                    <input type="text" className="form-control"
                            name="address"
                            value={this.state.address}
                            onChange={(e) => this.handleChange(e)}
                        />
                </div>
                  
                <button className="btn btn-primary" onClick={() => this.handleSubmit()}>Submit</button>
            </div>

            {this.showData()}
            </div>

    //         <div>
    //           <h1>Controlled Type Form</h1>
    //           <div id="form">
    //           <div id="name">
    //           <label>name:</label>
    //             <input type ="text"
    //             name="Employee_name"
    //             value={this.state.Employee_name}
    //             onChange={(e) => this.handleChange(e)}
    //             />
    //             <p style={{color: 'red'}}>{this.state.validationMessage}</p>
    //         </div>
    //         <div id="phone_no">
    //             <label>Phone Number</label>
    //             <input type="number"
    //             name="phone_no"
    //             values={this.state.phone_no}
    //             onChange={(e) => this.handleChange(e)}
    //             />
    //         </div>
    //         <div id="email">
    //             <label>email</label>
    //             <input type="email"
    //             name="email"
    //             values={this.state.email}
    //             onChange={(e) => this.handleChange(e)}
    //             />
    //         </div>
    //         <div id = 'address'>
    //                     <label>Address:</label>
    //                     <input type="text"
    //                         name="address"
    //                         value={this.state.address}
    //                         onChange={(e) => this.handleChange(e)}
    //                     />
    //                 </div>

    //     </div>
    //     <button onClick={() => this.handleSubmit()}>Submit</button>
    
        // {this.showData()}
    // </div>        
        );
    }
}

export default Form;
