import { Component } from 'react';
import Displayitem from './DisplayItem';

class DisplayComponent extends Component {
    render() {
        return (
            <div>
                <table className="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Phone number</th>
                        <th scope="col">Email</th>
                        <th scope="col">Address</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>

                <tbody>
                    {this.props.myArray.map((item) => {
                        return <Displayitem deleteItem={this.props.handleDelete} itemElements={item} />
                    })} 
                </tbody>
            </table>

            <p></p>
            </div>
        );
    }
}

export default DisplayComponent;